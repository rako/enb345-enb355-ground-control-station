﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Collections;
using ENB354_ENB355_GCS_COMMON;
using Vlc.DotNet.Core.Medias;
using System.Speech.Synthesis;

namespace ENB354_GCS {

    public partial class GCS_MAIN : Form {

        //todo timeout comms

        private enum GCS_State
        {
            Initial,
            Connecting,
            ConnectedJpeg,
            ConnectedRTP,
            ConnectedAdminJpeg,
            ConnectedAdminRTP
        }

        private enum BlimpState
        {
            OnGround,
            Searching,
            Circumnavigating,
            delivering,
            landedStart,
            ladedFinished,
        }

        private enum DeliverTo
        {
            Yellow = 0,
            Green = 1,
            Pink = 2,
            Now = 3,
            NONE
        }

        #region variables
        const int HeaderSize = 1 + 4;

        private static List<Data> info = new List<Data>();
        private static BindingList<Data> displayInfo = new BindingList<Data>();

        private static bool endThread = false;
        public static string BlimpId = string.Empty;
        public UInt16 sequenceNumber;
        Thread receve;
        Thread timeout;

        Thread controller;

        private static BindingList<Command> _commands = new BindingList<Command>();

        const string GCSName = "Group 4 - GCS";

        NetworkStream _clientStream;
        TcpClient _client = null;

        byte KeepAliveTimeInSeconds = 5;

        const UInt32 replyPacketSize = HeaderSize + 2;

        bool isAdmin = false;

        bool isJpeg = false;
        int JpegPos = -1;

        DeliverTo deliverTo = DeliverTo.NONE;

        DateTime latestData = DateTime.Now;

        #endregion

        #region form opening
        public GCS_MAIN() {
            InitializeComponent();
            dataGridViewData.AutoGenerateColumns = false;

            DataGridViewTextBoxColumn nameColumn = new DataGridViewTextBoxColumn();
            nameColumn.DataPropertyName = "Name";
            nameColumn.HeaderText = "Sensor Name";

            DataGridViewTextBoxColumn valueColumn = new DataGridViewTextBoxColumn();
            valueColumn.DataPropertyName = "Value";
            valueColumn.HeaderText = "Sensor Value";

            dataGridViewData.Columns.Add(nameColumn);
            dataGridViewData.Columns.Add(valueColumn);

            dataGridViewCommands.AutoGenerateColumns = false;

            DataGridViewTextBoxColumn sourceColumn = new DataGridViewTextBoxColumn();
            sourceColumn.DataPropertyName = "Source";
            sourceColumn.HeaderText = "Source";
            sourceColumn.FillWeight = 0.1f;
            sourceColumn.MinimumWidth = 45;

            DataGridViewTextBoxColumn timeColumn = new DataGridViewTextBoxColumn();
            timeColumn.DataPropertyName = "Time";
            timeColumn.HeaderText = "Time";
            timeColumn.FillWeight = 0.1f;
            timeColumn.MinimumWidth = 50;

            DataGridViewTextBoxColumn CommandColumn = new DataGridViewTextBoxColumn();
            CommandColumn.DataPropertyName = "CommandMessage";
            CommandColumn.HeaderText = "Command";
            CommandColumn.FillWeight = 100;

            dataGridViewCommands.Columns.Add(sourceColumn);
            dataGridViewCommands.Columns.Add(timeColumn);
            dataGridViewCommands.Columns.Add(CommandColumn);

            dataGridViewData.DataSource = displayInfo;
            dataGridViewCommands.DataSource = _commands;

            controller = new Thread(controllerThread);
            controller.Start();

            timeout = new Thread(timeoutThread);
            timeout.Start();

            ChangeState(GCS_State.Initial);
        }
        #endregion

        private void timeoutThread() {
            while (true)
            {
                double diff = (DateTime.Now - latestData).Duration().TotalMilliseconds;
                if (buttonConnect.Text == "disconect")
                {
                    if (diff >= KeepAliveTimeInSeconds * 1000)
                    {
                        labelConnectionStateUpdate(Color.Red, "Connection State: Timeout");
                    }
                    else
                    {
                        labelConnectionStateUpdate(Color.Green, "Connection State: Connected");
                    }
                }
                if (buttonConnect.Text == "disconect" && diff > 0 && diff < 3000)
                {
                    Thread.Sleep((int)diff);
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }
        }

        private delegate void labelConnectionStateUpdateDelegate(Color color, string text);
        private void labelConnectionStateUpdate(Color color, string text)
        {
            if (this.InvokeRequired)
            {
                this.EndInvoke(labelConnectionState.BeginInvoke(new labelConnectionStateUpdateDelegate(labelConnectionStateUpdate), new object[] { color, text }));
            }
            else
            {
                labelConnectionState.Text = text;
                labelConnectionState.ForeColor = color;
            }
        }

        #region gamePad
        float verticalLimit = 1;
        float horizontalLimit = 1;
        private void controllerThread()
        {
            GamepadState gState = new GamepadState(SlimDX.XInput.UserIndex.One);
            while (true)
            {
                gState.Update();
                float motorLeft = 0, motorRight = 0, motorUp = 0;
                motorUp = gState.LeftStick.Position.Y;
                if (gState.LeftTrigger == 0 && gState.RightTrigger == 0)
                {
                    motorLeft = 0;
                    motorRight = 0;
                }
                else if (gState.LeftTrigger == 0)
                {
                    motorLeft = gState.RightTrigger;
                    motorRight = gState.RightTrigger;
                }
                else if (gState.RightTrigger == 0)
                {
                    motorLeft = -1 * gState.LeftTrigger;
                    motorRight = -1 * gState.LeftTrigger;
                }

                if (gState.RightStick.Position.X > 0)
                {
                    if (gState.RightStick.Position.X < 0.5)
                    {
                        motorRight -= motorRight * gState.RightStick.Position.X * 2;
                    }
                    else
                    {
                        motorRight = -motorRight * ((gState.RightStick.Position.X - 0.5f) * 2.0f);
                    }
                }
                else if (gState.RightStick.Position.X < 0)
                {
                    //motorLeft += motorLeft * gState.RightStick.Position.X;
                    if (gState.RightStick.Position.X > -0.5)
                    {
                        motorLeft += motorLeft * gState.RightStick.Position.X * 2;
                    }
                    else
                    {
                        motorLeft *= ((gState.RightStick.Position.X + 0.5f) * 2.0f);
                    }
                }
                motorLeft *= horizontalLimit;
                motorRight *= horizontalLimit;
                motorUp *= verticalLimit;
                displayControllerInfo(gState, motorLeft, motorRight, motorUp);
                try
                {
                    if (_client != null && _client.Connected && !endThread && isAdmin)
                    {
                        byte[] message;
                        UInt32 size = 4 * 3 + HeaderSize;
                        message = new byte[size];
                        int pos = 0;
                        message[pos] = (byte)PacktHeader.Control;
                        pos += 1;
                        BitConverter.GetBytes(size).CopyTo(message, pos);
                        pos += 4;
                        BitConverter.GetBytes(motorLeft).CopyTo(message, pos);
                        pos += 4;
                        BitConverter.GetBytes(motorRight).CopyTo(message, pos);
                        pos += 4;
                        BitConverter.GetBytes(motorUp).CopyTo(message, pos);
                        pos += 4;
                        try
                        {
                            _clientStream.Write(message, 0, message.Length);
                        }
                        catch { }

                        if (deliverTo != DeliverTo.NONE)
                        {

                            size = 1 + HeaderSize;
                            message = new byte[size];
                            pos = 0;
                            message[pos] = (byte)PacktHeader.DeliverTo;
                            pos += 1;
                            BitConverter.GetBytes(size).CopyTo(message, pos);
                            pos += 4;
                            message[pos] = (byte)deliverTo;
                            deliverTo = DeliverTo.NONE;
                            try
                            {
                                _clientStream.Write(message, 0, message.Length);
                            }
                            catch { }
                        }
                    }

                }
                catch { }
                Thread.Sleep(20);
            }
        }

        private delegate void displayControllerInfoDelegate(GamepadState gState, float motorLeft, float motorRight, float motorUp);
        private void displayControllerInfo(GamepadState gState, float motorLeft, float motorRight, float motorUp)
        {
            if (this.InvokeRequired)
            {
                this.EndInvoke(dataGridViewData.BeginInvoke(new displayControllerInfoDelegate(displayControllerInfo), new object[] { gState, motorLeft, motorRight, motorUp }));
            }
            else
            {
                textBoxControllerAnalogLeftY.Text = gState.LeftStick.Position.Y.ToString();
                textBoxControllerAnalogRightX.Text = gState.RightStick.Position.X.ToString();
                textBoxControllerLeftTriger.Text = gState.LeftTrigger.ToString();
                textBoxControllerRightTriger.Text = gState.RightTrigger.ToString();

                textBoxMotorLeft.Text = motorLeft.ToString();
                textBoxMotorRight.Text = motorRight.ToString();
                textBoxMotorVertical.Text = motorUp.ToString();
            }
        }

        private void textBoxLimitHorizontal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                horizontalLimit = float.Parse(textBoxLimitHorizontal.Text);
            }
            catch
            {
                MessageBox.Show("Error: invalid limit must be between 1 and 0");
                textBoxLimitHorizontal.Text = horizontalLimit.ToString();
            }
        }

        private void textBoxLimitVertical_TextChanged(object sender, EventArgs e)
        {
            try
            {
                verticalLimit = float.Parse(textBoxLimitVertical.Text);
            }
            catch
            {
                MessageBox.Show("Error: invalid limit must be between 1 and 0");
                textBoxLimitVertical.Text = verticalLimit.ToString();
            }
        }
        #endregion

        #region form closing
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ChangeState(GCS_State.Initial);
            endThread = true;
            if (receve != null)
            {
                receve.Abort();
                while (receve.IsAlive)
                {
                    Thread.Sleep(20);
                }
            }
            if (controller != null)
            {
                controller.Abort();
                while (controller.IsAlive)
                {
                    Thread.Sleep(20);
                }
            }
            if (timeout != null)
            {
                timeout.Abort();
                while (timeout.IsAlive)
                {
                    Thread.Sleep(20);
                }
            }
        }
        #endregion

        #region recieve
        private void Recieve()
        {
            try
            {
                while (_client.Connected && !endThread)
                {
                    byte[] header = Helper.read(_clientStream, HeaderSize, ref latestData);
                    UInt32 size = BitConverter.ToUInt32(header, 1);
                    byte[] body = Helper.read(_clientStream, (int)size - HeaderSize, ref latestData);

                    switch ((PacktHeader)header[0])
                    {
                        case PacktHeader.SensorData:
                            //data Packet
                            ProcessSensorDataPacket(body);
                            break;
                        case PacktHeader.Commands:
                            //commands packet
                            ProcessCommandPacket(body);
                            break;
                        default:
                            throw new Exception("Incorrect packet header recived at this stage: " + header[0]);
                    }
                }
            }
            catch (Exception ex) 
            {
                //MessageBox.Show("Error: " + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ChangeState(GCS_State.Initial);

            //todo figure out a way of distinguishing a proper disconnect from a forced disconnect
        }
        #endregion

        #region process sensor data packet

        bool[] survivorDetected = {false, false, false};
        SpeechSynthesizer reader = new SpeechSynthesizer();

        private delegate void ProcessSensorDataPacketDelegate(byte[] packetBody);
        private void ProcessSensorDataPacket(byte[] packetBody)
        {
            if (dataGridViewData.InvokeRequired)
            {
                dataGridViewData.EndInvoke(dataGridViewData.BeginInvoke(new ProcessSensorDataPacketDelegate(ProcessSensorDataPacket), new object[] { packetBody }));
            }
            else
            {
                int pos = 0;

                //int displayInfoPos = 0;
                for (int i = 0; i < info.Count; ++i)
                {
                    info[i].Update(packetBody, ref pos);
                }
                //todo find more elegent soloution then "&& info.Count != 0"
                if (packetBody.Length != pos && info.Count != 0)
                {
                    throw new Exception("sensor packet has bytes left over");
                }

                //update jpeg
                if (isJpeg)
                {
                    pictureBoxImagery.Image = (Image)info[JpegPos]._value;
                }
                if (isAdmin) {
                    if ((string)info[5]._value == "No") {
                        survivorDetected[0] = false;
                    } else {
                        if (!survivorDetected[0]) {
                            survivorDetected[0] = true;
                            reader.SpeakAsync("Yellow Survivor Detected");
                        }
                    }
                    if ((string)info[6]._value == "No") {
                        survivorDetected[1] = false;
                    } else {
                        if (!survivorDetected[1]) {
                            survivorDetected[1] = true;
                            reader.SpeakAsync("Green Survivor Detected");
                        }
                    }

                    if ((string)info[7]._value == "No") {
                        survivorDetected[2] = false;
                    } else {
                        if (!survivorDetected[2]) {
                            survivorDetected[2] = true;
                            reader.SpeakAsync("Pink Survivor Detected");
                        }
                    }
                }
            }
        }
        #endregion

        #region process comms packet
        private delegate void ProcessCommandPacketDelegate(byte[] packetBody);
        private void ProcessCommandPacket(byte[] packetBody)
        {
            if (dataGridViewCommands.InvokeRequired)
            {
                dataGridViewCommands.EndInvoke(dataGridViewCommands.BeginInvoke(new ProcessCommandPacketDelegate(ProcessCommandPacket), new object[] { packetBody }));
            }
            else
            {
                int pos = 0;
                //todo
                if (packetBody.Length < 2)
                {
                    throw new Exception("comms packet not large enough");
                }
                UInt16 size = BitConverter.ToUInt16(packetBody, pos);
                pos += 2;
                if (packetBody.Length < pos + size * 2)
                {
                    throw new Exception("comms packet not large enough");
                }
                for (int i = 0; i < size; ++i)
                {
                    _commands.Insert(0, new Command(packetBody, ref pos));
                }
                if (packetBody.Length != pos)
                {
                    throw new Exception("comms packet has bytes left over");
                }
            }
        }
        #endregion

        #region connect and set up
        #region button connect
        private string comboboxServerSelected;
        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (buttonConnect.Text == "connect")
            {
                comboboxServerSelected = comboBoxServer.SelectedItem.ToString();
                Thread connect = new Thread(beginTransaction);
                connect.Start();
            }
            else if (buttonConnect.Text == "disconect")
            {
                ChangeState(GCS_State.Initial);
            }
            else
            {
                Debug.Assert(false, "connect button has wrong text");
            }
        }
        #endregion

        #region connect
        private void beginTransaction()
        {
            try
            {
                ChangeState(GCS_State.Connecting);

                string ip = null;
                int port = -1;
                int videoPort = -1;
                switch (comboboxServerSelected)
                {
                    case "Group 1":
                        ip = Properties.Settings.Default.Group1IP;
                        port = Properties.Settings.Default.Group1Port;
                        videoPort = Properties.Settings.Default.Group1Video;
                        break;
                    case "Group 2":
                        ip = Properties.Settings.Default.Group2IP;
                        port = Properties.Settings.Default.Group2Port;
                        videoPort = Properties.Settings.Default.Group2Video;
                        break;
                    case "Group 3":
                        ip = Properties.Settings.Default.Group3IP;
                        port = Properties.Settings.Default.Group3Port;
                        videoPort = Properties.Settings.Default.Group3Video;
                        break;
                    case "Group 4":
                        ip = Properties.Settings.Default.Group4IP;
                        port = Properties.Settings.Default.Group4Port;
                        videoPort = Properties.Settings.Default.Group4Video;
                        break;
                    case "Group 5":
                        ip = Properties.Settings.Default.Group5IP;
                        port = Properties.Settings.Default.Group5Port;
                        videoPort = Properties.Settings.Default.Group5Video;
                        break;
                    default:
                        Debug.Assert(false, "Somehow the selected group is not known");
                        break;
                }

                //connect
                EstablishConnection(ip, port);

                //send connection message
                SendLoginRequest();

                //recieve connection reply
                ConnectionReply();

                //set up data
                SetUpPacket();

                //start reciever
                receve = new Thread(Recieve);
                receve.Start();

                if (isAdmin)
                {
                    if (isJpeg)
                    {
                        ChangeState(GCS_State.ConnectedAdminJpeg);
                    }
                    else
                    {
                        ChangeState(GCS_State.ConnectedAdminRTP);
                        vlcControl.Media = new LocationMedia("rtp://@:"+ videoPort.ToString());
                    }
                }
                else if (isJpeg)
                {
                    ChangeState(GCS_State.ConnectedJpeg);
                }
                else
                {
                    ChangeState(GCS_State.ConnectedRTP);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ChangeState(GCS_State.Initial);
            }
        }
        #endregion

        #region establish connection
        private void EstablishConnection(string ip, int port)
        {
            _client = new TcpClient();

            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);

            _client.Connect(serverEndPoint);

            latestData = DateTime.Now;

            _clientStream = _client.GetStream();
        }
        #endregion

        #region send login request
        private void SendLoginRequest()
        {
            byte[] message;
            byte[] blimpName = Helper.StringToByteArray(GCSName);
            UInt32 size = (UInt32)blimpName.Length + HeaderSize;
            message = new byte[size];
            message[0] = (byte)PacktHeader.LoginRequest;
            BitConverter.GetBytes(size).CopyTo(message, 1);
            blimpName.CopyTo(message, HeaderSize);
            _clientStream.Write(message, 0, message.Length);
        }
        #endregion

        #region login reply
        private void ConnectionReply()
        {
            byte[] replyHeader = Helper.read(_clientStream, HeaderSize, ref latestData);
            if (replyHeader[0] != (byte)PacktHeader.ReplyLoginRequest)
            {
                throw new Exception("Incorrect packet header for reply login: " + replyHeader[0]);
            }
            UInt32 replySize = BitConverter.ToUInt32(replyHeader, 1);
            if (replySize != replyPacketSize)
            {
                throw new Exception("Incorrect Reply Packet Size");
            }
            byte[] replyBody = Helper.read(_clientStream, (int)replySize - HeaderSize, ref latestData);
            byte ErrorCode = replyBody[0];
            Debug.Assert(!isAdmin,"Should Not Already Be Admin");
            if ((byte)ErrorCodes.AdminLogin == ErrorCode)
            {
                isAdmin = true;
            }
            if (!(ErrorCode == (byte)ErrorCodes.NoError || ErrorCode == (byte)ErrorCodes.AdminLogin))
            {
                throw new Exception("Reply Packet Specifies Error: " + (ErrorCodes)ErrorCode);
            }
            KeepAliveTimeInSeconds = replyBody[1];
        }
        #endregion

        #region set up packet
        private void SetUpPacket()
        {
            Debug.Assert(!isJpeg, "should not already be determined to use Jpeg");
            //set up data
            byte[] setUpHeader = Helper.read(_clientStream, HeaderSize, ref latestData);
            if (setUpHeader[0] != (byte)PacktHeader.SensorDataStructure)
            {
                throw new Exception("Incorrect packet for data setup packet: " + setUpHeader[0]);
            }
            UInt32 setUpSize = BitConverter.ToUInt32(setUpHeader, 1);
            byte[] setUpBody = Helper.read(_clientStream, (int)setUpSize - HeaderSize, ref latestData);

            int pos = 0;
            int floatCount = 0;
            int intCount = 0;
            byte NumData = setUpBody[pos];
            pos++;
            if (NumData + pos > setUpBody.Length)
            {
                throw new Exception("set up packet, packet size is too small to hold the sensor types");
            }
            byte[] dataTypes = new byte[NumData];
            for (int i = 0; i < dataTypes.Length; ++i)
            {
                dataTypes[i] = setUpBody[pos];
                if ((Data.DataType)dataTypes[i] == Data.DataType.SI32)
                {
                    intCount++;
                }
                else if ((Data.DataType)dataTypes[i] == Data.DataType.FLOAT)
                {
                    floatCount++;
                }
                else if ((Data.DataType)dataTypes[i] == Data.DataType.JPEG)
                {
                    if (isJpeg)
                    {
                        throw new Exception("More then one JPEG Specified");
                    }
                    else
                    {
                        isJpeg = true;
                        JpegPos = i;
                    }
                }
                pos++;
            }

            if (NumData + pos + intCount * 2 * 4 + floatCount * 2 * 4 > setUpBody.Length)
            {
                throw new Exception("set up packet, packet size is too small to hold mins and maximums");
            }
            int[] minIntValues = new int[intCount];
            int[] maxIntValues = new int[intCount];

            float[] minFloatValues = new float[floatCount];
            float[] maxFloatValues = new float[floatCount];

            int intPos = 0;
            int floatPos = 0;
            for (int i = 0; i < dataTypes.Length; ++i)
            {
                if ((Data.DataType)dataTypes[i] == Data.DataType.SI32)
                {
                    minIntValues[intPos] = BitConverter.ToInt32(setUpBody, pos);
                    pos += 4;
                    maxIntValues[intPos] = BitConverter.ToInt32(setUpBody, pos);
                    pos += 4;
                    intPos++;
                }
                else if ((Data.DataType)dataTypes[i] == Data.DataType.FLOAT)
                {
                    minFloatValues[floatPos] = BitConverter.ToSingle(setUpBody, pos);
                    pos += 4;
                    maxFloatValues[floatPos] = BitConverter.ToSingle(setUpBody, pos);
                    pos += 4;
                    floatPos++;
                }

            }
            if (NumData + pos + NumData > setUpBody.Length)
            {
                throw new Exception("set up packet, packet size is too small to hold data names");
            }
            string[] Names = new string[NumData];
            for (int i = 0; i < Names.Length; ++i)
            {
                Names[i] = Helper.getStringFromBytes(setUpBody, ref pos);
            }

            if (pos != setUpBody.Length)
            {
                throw new Exception("There are left over bits at the end of the set up packet");
            }

            

            //get sensor data (info) list ready
            SetUpInfo(NumData, dataTypes, minIntValues, maxIntValues, minFloatValues, maxFloatValues, Names);
        }

        private delegate void SetUpInfoDelegate(byte NumData, byte[] dataTypes, int[] minIntValues, int[] maxIntValues, float[] minFloatValues, float[] maxFloatValues, string[] Names);
        private void SetUpInfo(byte NumData, byte[] dataTypes, int[] minIntValues, int[] maxIntValues, float[] minFloatValues, float[] maxFloatValues, string[] Names)
        {
            if (dataGridViewData.InvokeRequired)
            {
                dataGridViewData.EndInvoke(dataGridViewData.BeginInvoke(new SetUpInfoDelegate(SetUpInfo), new object[] { NumData, dataTypes, minIntValues, maxIntValues, minFloatValues, maxFloatValues, Names }));
            }
            else
            {
                int intPos = 0;
                int floatPos = 0;
                info.Capacity = NumData;
                for (int i = 0; i < NumData; ++i)
                {
                    if ((Data.DataType)dataTypes[i] == Data.DataType.JPEG)
                    {
                        info.Add(new Data((Data.DataType)dataTypes[i], Names[i], true));
                    }
                    else if ((Data.DataType)dataTypes[i] == Data.DataType.SI32)
                    {
                        info.Add(new Data(minIntValues[intPos], maxIntValues[intPos], Names[i]));
                        intPos++;
                    }
                    else if ((Data.DataType)dataTypes[i] == Data.DataType.FLOAT)
                    {
                        info.Add(new Data(minFloatValues[floatPos], maxFloatValues[floatPos], Names[i]));
                        floatPos++;
                    }
                    else
                    {
                        info.Add(new Data((Data.DataType)dataTypes[i], Names[i]));
                    }
                }

                setUpDataDisplay();
            }
        }

        private delegate void setUpDataDisplayDelegate();
        private  void setUpDataDisplay()
        {
            if (dataGridViewData.InvokeRequired)
            {
                dataGridViewData.EndInvoke(dataGridViewData.BeginInvoke(new setUpDataDisplayDelegate(setUpDataDisplay)));
            }
            else
            {
                for (int i = 0; i < info.Count; ++i)
                {
                    if (!info[i].hidden)
                    {
                        displayInfo.Add(info[i]);
                    }
                }
            }
        }
        #endregion
        #endregion

        #region gui stuff
        #region change gui state

        const string DefaultTitle = "Group 4 - ZEPHYR - Ground Control Station";

        private delegate void ChangeStateDelegate(GCS_State state);
        private void ChangeState(GCS_State state)
        {
            if (this.InvokeRequired)
            {
                this.EndInvoke(this.BeginInvoke(new ChangeStateDelegate(ChangeState), new object[] { state }));
            }
            else
            {
                switch (state)
                {
                        //todo allow gcs to select rtp port
                    case GCS_State.Initial:
                        isJpeg = false;
                        isAdmin = false;
                        comboBoxServer.Enabled = true;
                        buttonDY.Enabled = false;
                        buttonDG.Enabled = false;
                        buttonDP.Enabled = false;
                        buttonDN.Enabled = false;
                        buttonSettings.Enabled = true;
                        buttonConnect.Enabled = true;
                        _commands.Clear();
                        info.Clear();
                        displayInfo.Clear();
                        if (_client != null)
                        {
                            _client.Close();
                        }
                        if (_clientStream != null)
                        {
                            _clientStream.Close();
                        }
                        endThread = true;
                        buttonConnect.Text = "connect";
                        enableConnectButtonSelectivley();
                        vlcControl.Stop();
                        vlcControl.Visible = false;
                        pictureBoxImagery.Visible = true;
                        KeepAliveTimeInSeconds = 5;
                        labelConnectionState.Text = "Connection State: Not Connected";
                        labelConnectionState.ForeColor = SystemColors.ControlText;
                        this.Text = DefaultTitle;
                        break;
                    case GCS_State.Connecting:
                        buttonSettings.Enabled = false;
                        buttonConnect.Enabled = false;
                        comboBoxServer.Enabled = false;
                        endThread = false;
                        latestData = DateTime.Now;
                        labelConnectionState.Text = "Connection State: Connecting";
                        break;
                    case GCS_State.ConnectedJpeg:
                        buttonConnect.Text = "disconect";
                        buttonConnect.Enabled = true;
                        this.Text = DefaultTitle + " (MGCS)";
                        //todo
                        break;
                    case GCS_State.ConnectedRTP:
                        buttonConnect.Text = "disconect";
                        buttonConnect.Enabled = true;
                        vlcControl.Visible = true;
                        pictureBoxImagery.Visible = false;
                        this.Text = DefaultTitle + " (MGCS)";
                        //todo
                        break;
                    case GCS_State.ConnectedAdminJpeg:
                        buttonConnect.Text = "disconect";
                        buttonConnect.Enabled = true;
                        //buttonCicumnavigate.Enabled = true;
                        //buttonSearch.Enabled = true;
                        buttonDY.Enabled = true;
                        buttonDG.Enabled = true;
                        buttonDP.Enabled = true;
                        buttonDN.Enabled = true;
                        this.Text = DefaultTitle + " (Admin)";
                        break;
                    case GCS_State.ConnectedAdminRTP:
                        buttonConnect.Text = "disconect";
                        buttonConnect.Enabled = true;
                        //buttonCicumnavigate.Enabled = true;
                        //buttonSearch.Enabled = true;
                        buttonDY.Enabled = true;
                        buttonDG.Enabled = true;
                        buttonDP.Enabled = true;
                        buttonDN.Enabled = true;
                        vlcControl.Visible = true;
                        pictureBoxImagery.Visible = false;
                        this.Text = DefaultTitle + " (Admin)";
                        //todo
                        break;
                    default:
                        Debug.Assert(false, "Changing of GCS State Invalid");
                        break;
                }
            }
        }
        #endregion

        #region settings button
        private void buttonSettings_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
        }
        #endregion

        #region selected blimp stuff
        #region selected blimp changed
        private void comboBoxServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            enableConnectButtonSelectivley();
        }
        #endregion

        #region connect button avalability logic
        private void enableConnectButtonSelectivley()
        {
            if (comboBoxServer.SelectedIndex >= 0)
            {
                buttonConnect.Enabled = true;
            }
            else
            {
                buttonConnect.Enabled = false;
            }
        }
        #endregion
        #endregion
        #endregion

        bool fullScreen = false;

        private void pictureBoxImagery_DoubleClick(object sender, EventArgs e)
        {
            if (fullScreen)
            {
                this.Controls.Remove(this.pictureBoxImagery);
                this.panel5.Controls.Add(this.pictureBoxImagery);
                pictureBoxImagery.Dock = DockStyle.Fill;
                
                fullScreen = false;
            }
            else
            {
                panel5.Controls.Remove(this.pictureBoxImagery);
                this.Controls.Add(this.pictureBoxImagery);
                pictureBoxImagery.Dock = DockStyle.Fill;
                pictureBoxImagery.BringToFront();
                fullScreen = true;
            }
        }

        private void buttonDY_Click(object sender, EventArgs e)
        {
            deliverTo = DeliverTo.Yellow;
        }

        private void buttonDG_Click(object sender, EventArgs e)
        {
            deliverTo = DeliverTo.Green;
        }

        private void buttonDP_Click(object sender, EventArgs e)
        {
            deliverTo = DeliverTo.Pink;
        }

        private void buttonDN_Click(object sender, EventArgs e)
        {
            deliverTo = DeliverTo.Now;
        }
    }
}
