﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace ENB354_GCS
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
            textBoxIPGroup1.Text = Properties.Settings.Default.Group1IP;
            textBoxIPGroup2.Text = Properties.Settings.Default.Group2IP;
            textBoxIPGroup3.Text = Properties.Settings.Default.Group3IP;
            textBoxIPGroup4.Text = Properties.Settings.Default.Group4IP;
            textBoxIPGroup5.Text = Properties.Settings.Default.Group5IP;

            textBoxPort1.Text = Properties.Settings.Default.Group1Port.ToString();
            textBoxPort2.Text = Properties.Settings.Default.Group2Port.ToString();
            textBoxPort3.Text = Properties.Settings.Default.Group3Port.ToString();
            textBoxPort4.Text = Properties.Settings.Default.Group4Port.ToString();
            textBoxPort5.Text = Properties.Settings.Default.Group5Port.ToString();

            textBoxVideo1.Text = Properties.Settings.Default.Group1Video.ToString();
            textBoxVideo2.Text = Properties.Settings.Default.Group2Video.ToString();
            textBoxVideo3.Text = Properties.Settings.Default.Group3Video.ToString();
            textBoxVideo4.Text = Properties.Settings.Default.Group4Video.ToString();
            textBoxVideo5.Text = Properties.Settings.Default.Group5Video.ToString();
        }

        private void checkIp(TextBox textBox, int groupNumber)
        {
            IPAddress temp;
            if (!IPAddress.TryParse(textBox.Text, out temp))
            {
                throw new Exception("Group " + groupNumber + " Ip Address is invalid");
            }
        }

        private void checkPort(TextBox textBox, int groupNumber)
        {
            UInt16 temp;
            if (!UInt16.TryParse(textBox.Text, out temp))
            {
                throw new Exception("Group " + groupNumber + " port number is invalid");
            }
        }

        private void checkVideoPort(TextBox textBox, int groupNumber)
        {
            UInt16 temp;
            if (!UInt16.TryParse(textBox.Text, out temp))
            {
                throw new Exception("Group " + groupNumber + " video port number is invalid");
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                //check inputs
                checkIp(textBoxIPGroup1, 1);
                checkIp(textBoxIPGroup2, 2);
                checkIp(textBoxIPGroup3, 3);
                checkIp(textBoxIPGroup4, 4);
                checkIp(textBoxIPGroup5, 5);

                checkPort(textBoxPort1, 1);
                checkPort(textBoxPort2, 2);
                checkPort(textBoxPort3, 3);
                checkPort(textBoxPort4, 4);
                checkPort(textBoxPort5, 5);

                checkVideoPort(textBoxVideo1, 1);
                checkVideoPort(textBoxVideo2, 2);
                checkVideoPort(textBoxVideo3, 3);
                checkVideoPort(textBoxVideo4, 4);
                checkVideoPort(textBoxVideo5, 5);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            
            //todo save
            Properties.Settings.Default.Group1IP = textBoxIPGroup1.Text;
            Properties.Settings.Default.Group2IP = textBoxIPGroup2.Text;
            Properties.Settings.Default.Group3IP = textBoxIPGroup3.Text;
            Properties.Settings.Default.Group4IP = textBoxIPGroup4.Text;
            Properties.Settings.Default.Group5IP = textBoxIPGroup5.Text;

            Properties.Settings.Default.Group1Port = int.Parse(textBoxPort1.Text);
            Properties.Settings.Default.Group2Port = int.Parse(textBoxPort2.Text);
            Properties.Settings.Default.Group3Port = int.Parse(textBoxPort3.Text);
            Properties.Settings.Default.Group4Port = int.Parse(textBoxPort4.Text);
            Properties.Settings.Default.Group5Port = int.Parse(textBoxPort5.Text);

            Properties.Settings.Default.Group1Video = int.Parse(textBoxVideo1.Text);
            Properties.Settings.Default.Group2Video = int.Parse(textBoxVideo2.Text);
            Properties.Settings.Default.Group3Video = int.Parse(textBoxVideo3.Text);
            Properties.Settings.Default.Group4Video = int.Parse(textBoxVideo4.Text);
            Properties.Settings.Default.Group5Video = int.Parse(textBoxVideo5.Text);


            Properties.Settings.Default.Save();

            this.Close();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
