﻿namespace ENB354_GCS
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxIPGroup1 = new System.Windows.Forms.TextBox();
            this.textBoxPort1 = new System.Windows.Forms.TextBox();
            this.textBoxIPGroup2 = new System.Windows.Forms.TextBox();
            this.textBoxPort2 = new System.Windows.Forms.TextBox();
            this.textBoxIPGroup3 = new System.Windows.Forms.TextBox();
            this.textBoxPort3 = new System.Windows.Forms.TextBox();
            this.textBoxIPGroup4 = new System.Windows.Forms.TextBox();
            this.textBoxPort4 = new System.Windows.Forms.TextBox();
            this.textBoxIPGroup5 = new System.Windows.Forms.TextBox();
            this.textBoxPort5 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxVideo1 = new System.Windows.Forms.TextBox();
            this.textBoxVideo2 = new System.Windows.Forms.TextBox();
            this.textBoxVideo3 = new System.Windows.Forms.TextBox();
            this.textBoxVideo4 = new System.Windows.Forms.TextBox();
            this.textBoxVideo5 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.textBoxIPGroup1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxPort1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxIPGroup2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxPort2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxIPGroup3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxPort3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxIPGroup4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxPort4, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxIPGroup5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBoxPort5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label8, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxVideo1, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxVideo2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxVideo3, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxVideo4, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxVideo5, 3, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(302, 192);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBoxIPGroup1
            // 
            this.textBoxIPGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxIPGroup1.Location = new System.Drawing.Point(63, 28);
            this.textBoxIPGroup1.Name = "textBoxIPGroup1";
            this.textBoxIPGroup1.Size = new System.Drawing.Size(139, 20);
            this.textBoxIPGroup1.TabIndex = 0;
            this.textBoxIPGroup1.Text = "192.168.1.10";
            // 
            // textBoxPort1
            // 
            this.textBoxPort1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPort1.Location = new System.Drawing.Point(208, 28);
            this.textBoxPort1.Name = "textBoxPort1";
            this.textBoxPort1.Size = new System.Drawing.Size(42, 20);
            this.textBoxPort1.TabIndex = 1;
            this.textBoxPort1.Text = "55866";
            // 
            // textBoxIPGroup2
            // 
            this.textBoxIPGroup2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxIPGroup2.Location = new System.Drawing.Point(63, 53);
            this.textBoxIPGroup2.Name = "textBoxIPGroup2";
            this.textBoxIPGroup2.Size = new System.Drawing.Size(139, 20);
            this.textBoxIPGroup2.TabIndex = 2;
            this.textBoxIPGroup2.Text = "192.168.1.20";
            // 
            // textBoxPort2
            // 
            this.textBoxPort2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPort2.Location = new System.Drawing.Point(208, 53);
            this.textBoxPort2.Name = "textBoxPort2";
            this.textBoxPort2.Size = new System.Drawing.Size(42, 20);
            this.textBoxPort2.TabIndex = 3;
            this.textBoxPort2.Text = "55866";
            // 
            // textBoxIPGroup3
            // 
            this.textBoxIPGroup3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxIPGroup3.Location = new System.Drawing.Point(63, 78);
            this.textBoxIPGroup3.Name = "textBoxIPGroup3";
            this.textBoxIPGroup3.Size = new System.Drawing.Size(139, 20);
            this.textBoxIPGroup3.TabIndex = 4;
            this.textBoxIPGroup3.Text = "192.168.1.30";
            // 
            // textBoxPort3
            // 
            this.textBoxPort3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPort3.Location = new System.Drawing.Point(208, 78);
            this.textBoxPort3.Name = "textBoxPort3";
            this.textBoxPort3.Size = new System.Drawing.Size(42, 20);
            this.textBoxPort3.TabIndex = 5;
            this.textBoxPort3.Text = "55866";
            // 
            // textBoxIPGroup4
            // 
            this.textBoxIPGroup4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxIPGroup4.Location = new System.Drawing.Point(63, 103);
            this.textBoxIPGroup4.Name = "textBoxIPGroup4";
            this.textBoxIPGroup4.Size = new System.Drawing.Size(139, 20);
            this.textBoxIPGroup4.TabIndex = 6;
            this.textBoxIPGroup4.Text = "192.168.1.40";
            // 
            // textBoxPort4
            // 
            this.textBoxPort4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPort4.Location = new System.Drawing.Point(208, 103);
            this.textBoxPort4.Name = "textBoxPort4";
            this.textBoxPort4.Size = new System.Drawing.Size(42, 20);
            this.textBoxPort4.TabIndex = 7;
            this.textBoxPort4.Text = "55866";
            // 
            // textBoxIPGroup5
            // 
            this.textBoxIPGroup5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxIPGroup5.Location = new System.Drawing.Point(63, 128);
            this.textBoxIPGroup5.Name = "textBoxIPGroup5";
            this.textBoxIPGroup5.Size = new System.Drawing.Size(139, 20);
            this.textBoxIPGroup5.TabIndex = 8;
            this.textBoxIPGroup5.Text = "192.168.1.50";
            // 
            // textBoxPort5
            // 
            this.textBoxPort5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPort5.Location = new System.Drawing.Point(208, 128);
            this.textBoxPort5.Name = "textBoxPort5";
            this.textBoxPort5.Size = new System.Drawing.Size(42, 20);
            this.textBoxPort5.TabIndex = 9;
            this.textBoxPort5.Text = "55866";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Group 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Group 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Group 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Group 4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Group 5";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 0);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label6.Size = new System.Drawing.Size(139, 18);
            this.label6.TabIndex = 15;
            this.label6.Text = "Ip Address";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(208, 0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label7.Size = new System.Drawing.Size(42, 18);
            this.label7.TabIndex = 16;
            this.label7.Text = "Port";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(63, 153);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(187, 36);
            this.panel1.TabIndex = 17;
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(81, 4);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(0, 4);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(256, 0);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label8.Size = new System.Drawing.Size(43, 18);
            this.label8.TabIndex = 18;
            this.label8.Text = "Video";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxVideo1
            // 
            this.textBoxVideo1.Location = new System.Drawing.Point(256, 28);
            this.textBoxVideo1.Name = "textBoxVideo1";
            this.textBoxVideo1.Size = new System.Drawing.Size(42, 20);
            this.textBoxVideo1.TabIndex = 19;
            this.textBoxVideo1.Text = "55866";
            // 
            // textBoxVideo2
            // 
            this.textBoxVideo2.Location = new System.Drawing.Point(256, 53);
            this.textBoxVideo2.Name = "textBoxVideo2";
            this.textBoxVideo2.Size = new System.Drawing.Size(42, 20);
            this.textBoxVideo2.TabIndex = 20;
            this.textBoxVideo2.Text = "55866";
            // 
            // textBoxVideo3
            // 
            this.textBoxVideo3.Location = new System.Drawing.Point(256, 78);
            this.textBoxVideo3.Name = "textBoxVideo3";
            this.textBoxVideo3.Size = new System.Drawing.Size(42, 20);
            this.textBoxVideo3.TabIndex = 21;
            this.textBoxVideo3.Text = "55866";
            // 
            // textBoxVideo4
            // 
            this.textBoxVideo4.Location = new System.Drawing.Point(256, 103);
            this.textBoxVideo4.Name = "textBoxVideo4";
            this.textBoxVideo4.Size = new System.Drawing.Size(42, 20);
            this.textBoxVideo4.TabIndex = 22;
            this.textBoxVideo4.Text = "55866";
            // 
            // textBoxVideo5
            // 
            this.textBoxVideo5.Location = new System.Drawing.Point(256, 128);
            this.textBoxVideo5.Name = "textBoxVideo5";
            this.textBoxVideo5.Size = new System.Drawing.Size(42, 20);
            this.textBoxVideo5.TabIndex = 23;
            this.textBoxVideo5.Text = "55866";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 192);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Settings";
            this.Text = "Settings";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBoxIPGroup1;
        private System.Windows.Forms.TextBox textBoxPort1;
        private System.Windows.Forms.TextBox textBoxIPGroup2;
        private System.Windows.Forms.TextBox textBoxPort2;
        private System.Windows.Forms.TextBox textBoxIPGroup3;
        private System.Windows.Forms.TextBox textBoxPort3;
        private System.Windows.Forms.TextBox textBoxIPGroup4;
        private System.Windows.Forms.TextBox textBoxPort4;
        private System.Windows.Forms.TextBox textBoxIPGroup5;
        private System.Windows.Forms.TextBox textBoxPort5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxVideo1;
        private System.Windows.Forms.TextBox textBoxVideo2;
        private System.Windows.Forms.TextBox textBoxVideo3;
        private System.Windows.Forms.TextBox textBoxVideo4;
        private System.Windows.Forms.TextBox textBoxVideo5;
    }
}