﻿namespace ENB354_GCS {
    partial class GCS_MAIN {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dataGridViewData = new System.Windows.Forms.DataGridView();
            this.dataGridViewCommands = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.comboBoxServer = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDN = new System.Windows.Forms.Button();
            this.buttonDP = new System.Windows.Forms.Button();
            this.buttonDG = new System.Windows.Forms.Button();
            this.buttonDY = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBoxImagery = new System.Windows.Forms.PictureBox();
            this.vlcControl = new Vlc.DotNet.Forms.VlcControl();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxLimitVertical = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxLimitHorizontal = new System.Windows.Forms.TextBox();
            this.labelMotorVertical = new System.Windows.Forms.Label();
            this.textBoxMotorVertical = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxMotorRight = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxMotorLeft = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxControllerAnalogRightX = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxControllerAnalogLeftY = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxControllerRightTriger = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxControllerLeftTriger = new System.Windows.Forms.TextBox();
            this.labelConnectionState = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCommands)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImagery)).BeginInit();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewData
            // 
            this.dataGridViewData.AllowUserToAddRows = false;
            this.dataGridViewData.AllowUserToDeleteRows = false;
            this.dataGridViewData.AllowUserToOrderColumns = true;
            this.dataGridViewData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewData.Location = new System.Drawing.Point(413, 3);
            this.dataGridViewData.Name = "dataGridViewData";
            this.dataGridViewData.ReadOnly = true;
            this.dataGridViewData.RowHeadersVisible = false;
            this.dataGridViewData.Size = new System.Drawing.Size(273, 304);
            this.dataGridViewData.TabIndex = 0;
            // 
            // dataGridViewCommands
            // 
            this.dataGridViewCommands.AllowUserToAddRows = false;
            this.dataGridViewCommands.AllowUserToDeleteRows = false;
            this.dataGridViewCommands.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCommands.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCommands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCommands.Location = new System.Drawing.Point(413, 313);
            this.dataGridViewCommands.Name = "dataGridViewCommands";
            this.dataGridViewCommands.ReadOnly = true;
            this.dataGridViewCommands.RowHeadersVisible = false;
            this.dataGridViewCommands.Size = new System.Drawing.Size(273, 198);
            this.dataGridViewCommands.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 410F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.26943F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.73057F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewCommands, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewData, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 310F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1182, 544);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelConnectionState);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 517);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 24);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonSettings);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(692, 517);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(487, 24);
            this.panel2.TabIndex = 8;
            // 
            // buttonSettings
            // 
            this.buttonSettings.Location = new System.Drawing.Point(408, 0);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonSettings.TabIndex = 6;
            this.buttonSettings.Text = "Settings";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonConnect);
            this.panel3.Controls.Add(this.comboBoxServer);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(413, 517);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(273, 24);
            this.panel3.TabIndex = 9;
            // 
            // buttonConnect
            // 
            this.buttonConnect.Enabled = false;
            this.buttonConnect.Location = new System.Drawing.Point(3, 1);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(75, 23);
            this.buttonConnect.TabIndex = 1;
            this.buttonConnect.Text = "connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // comboBoxServer
            // 
            this.comboBoxServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxServer.FormattingEnabled = true;
            this.comboBoxServer.Items.AddRange(new object[] {
            "Group 4",
            "Group 1",
            "Group 2",
            "Group 3",
            "Group 5"});
            this.comboBoxServer.Location = new System.Drawing.Point(114, 0);
            this.comboBoxServer.Name = "comboBoxServer";
            this.comboBoxServer.Size = new System.Drawing.Size(157, 21);
            this.comboBoxServer.TabIndex = 0;
            this.comboBoxServer.SelectedIndexChanged += new System.EventHandler(this.comboBoxServer_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.buttonDN);
            this.panel4.Controls.Add(this.buttonDP);
            this.panel4.Controls.Add(this.buttonDG);
            this.panel4.Controls.Add(this.buttonDY);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 313);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(404, 198);
            this.panel4.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Deliver To:";
            // 
            // buttonDN
            // 
            this.buttonDN.Location = new System.Drawing.Point(177, 106);
            this.buttonDN.Name = "buttonDN";
            this.buttonDN.Size = new System.Drawing.Size(75, 23);
            this.buttonDN.TabIndex = 3;
            this.buttonDN.Text = "Now";
            this.buttonDN.UseVisualStyleBackColor = true;
            this.buttonDN.Click += new System.EventHandler(this.buttonDN_Click);
            // 
            // buttonDP
            // 
            this.buttonDP.Location = new System.Drawing.Point(39, 106);
            this.buttonDP.Name = "buttonDP";
            this.buttonDP.Size = new System.Drawing.Size(75, 23);
            this.buttonDP.TabIndex = 2;
            this.buttonDP.Text = "Pink";
            this.buttonDP.UseVisualStyleBackColor = true;
            this.buttonDP.Click += new System.EventHandler(this.buttonDP_Click);
            // 
            // buttonDG
            // 
            this.buttonDG.Location = new System.Drawing.Point(177, 52);
            this.buttonDG.Name = "buttonDG";
            this.buttonDG.Size = new System.Drawing.Size(75, 23);
            this.buttonDG.TabIndex = 1;
            this.buttonDG.Text = "Green";
            this.buttonDG.UseVisualStyleBackColor = true;
            this.buttonDG.Click += new System.EventHandler(this.buttonDG_Click);
            // 
            // buttonDY
            // 
            this.buttonDY.Location = new System.Drawing.Point(39, 52);
            this.buttonDY.Name = "buttonDY";
            this.buttonDY.Size = new System.Drawing.Size(75, 23);
            this.buttonDY.TabIndex = 0;
            this.buttonDY.Text = "Yellow";
            this.buttonDY.UseVisualStyleBackColor = true;
            this.buttonDY.Click += new System.EventHandler(this.buttonDY_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBoxImagery);
            this.panel5.Controls.Add(this.vlcControl);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(404, 304);
            this.panel5.TabIndex = 11;
            // 
            // pictureBoxImagery
            // 
            this.pictureBoxImagery.Image = global::ENB354_GCS.Properties.Resources._640x480;
            this.pictureBoxImagery.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxImagery.Name = "pictureBoxImagery";
            this.pictureBoxImagery.Size = new System.Drawing.Size(400, 294);
            this.pictureBoxImagery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxImagery.TabIndex = 4;
            this.pictureBoxImagery.TabStop = false;
            this.pictureBoxImagery.DoubleClick += new System.EventHandler(this.pictureBoxImagery_DoubleClick);
            // 
            // vlcControl
            // 
            this.vlcControl.Location = new System.Drawing.Point(3, 3);
            this.vlcControl.Name = "vlcControl";
            this.vlcControl.Rate = 0F;
            this.vlcControl.Size = new System.Drawing.Size(398, 298);
            this.vlcControl.TabIndex = 0;
            this.vlcControl.Text = "vlcControl1";
            this.vlcControl.Visible = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.textBoxLimitVertical);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.textBoxLimitHorizontal);
            this.panel6.Controls.Add(this.labelMotorVertical);
            this.panel6.Controls.Add(this.textBoxMotorVertical);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.textBoxMotorRight);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.textBoxMotorLeft);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.textBoxControllerAnalogRightX);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.textBoxControllerAnalogLeftY);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.textBoxControllerRightTriger);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.textBoxControllerLeftTriger);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(692, 3);
            this.panel6.Name = "panel6";
            this.tableLayoutPanel1.SetRowSpan(this.panel6, 2);
            this.panel6.Size = new System.Drawing.Size(487, 508);
            this.panel6.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "limit Up Down";
            // 
            // textBoxLimitVertical
            // 
            this.textBoxLimitVertical.Location = new System.Drawing.Point(137, 59);
            this.textBoxLimitVertical.Name = "textBoxLimitVertical";
            this.textBoxLimitVertical.Size = new System.Drawing.Size(100, 20);
            this.textBoxLimitVertical.TabIndex = 16;
            this.textBoxLimitVertical.Text = "1.0";
            this.textBoxLimitVertical.TextChanged += new System.EventHandler(this.textBoxLimitVertical_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "limit Ford Back";
            // 
            // textBoxLimitHorizontal
            // 
            this.textBoxLimitHorizontal.Location = new System.Drawing.Point(137, 23);
            this.textBoxLimitHorizontal.Name = "textBoxLimitHorizontal";
            this.textBoxLimitHorizontal.Size = new System.Drawing.Size(100, 20);
            this.textBoxLimitHorizontal.TabIndex = 14;
            this.textBoxLimitHorizontal.Text = "1.0";
            this.textBoxLimitHorizontal.TextChanged += new System.EventHandler(this.textBoxLimitHorizontal_TextChanged);
            // 
            // labelMotorVertical
            // 
            this.labelMotorVertical.AutoSize = true;
            this.labelMotorVertical.Location = new System.Drawing.Point(263, 292);
            this.labelMotorVertical.Name = "labelMotorVertical";
            this.labelMotorVertical.Size = new System.Drawing.Size(72, 13);
            this.labelMotorVertical.TabIndex = 13;
            this.labelMotorVertical.Text = "Veritcal Motor";
            // 
            // textBoxMotorVertical
            // 
            this.textBoxMotorVertical.Location = new System.Drawing.Point(378, 289);
            this.textBoxMotorVertical.Name = "textBoxMotorVertical";
            this.textBoxMotorVertical.Size = new System.Drawing.Size(100, 20);
            this.textBoxMotorVertical.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(263, 256);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "right motor";
            // 
            // textBoxMotorRight
            // 
            this.textBoxMotorRight.Location = new System.Drawing.Point(378, 253);
            this.textBoxMotorRight.Name = "textBoxMotorRight";
            this.textBoxMotorRight.Size = new System.Drawing.Size(100, 20);
            this.textBoxMotorRight.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(263, 225);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Left Motor";
            // 
            // textBoxMotorLeft
            // 
            this.textBoxMotorLeft.Location = new System.Drawing.Point(378, 222);
            this.textBoxMotorLeft.Name = "textBoxMotorLeft";
            this.textBoxMotorLeft.Size = new System.Drawing.Size(100, 20);
            this.textBoxMotorLeft.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(269, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "controllerAnalgoRightX";
            // 
            // textBoxControllerAnalogRightX
            // 
            this.textBoxControllerAnalogRightX.Location = new System.Drawing.Point(384, 103);
            this.textBoxControllerAnalogRightX.Name = "textBoxControllerAnalogRightX";
            this.textBoxControllerAnalogRightX.Size = new System.Drawing.Size(100, 20);
            this.textBoxControllerAnalogRightX.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "controllerAnalgoLeftY";
            // 
            // textBoxControllerAnalogLeftY
            // 
            this.textBoxControllerAnalogLeftY.Location = new System.Drawing.Point(384, 67);
            this.textBoxControllerAnalogLeftY.Name = "textBoxControllerAnalogLeftY";
            this.textBoxControllerAnalogLeftY.Size = new System.Drawing.Size(100, 20);
            this.textBoxControllerAnalogLeftY.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(269, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "controllerRightTriger";
            // 
            // textBoxControllerRightTriger
            // 
            this.textBoxControllerRightTriger.Location = new System.Drawing.Point(384, 36);
            this.textBoxControllerRightTriger.Name = "textBoxControllerRightTriger";
            this.textBoxControllerRightTriger.Size = new System.Drawing.Size(100, 20);
            this.textBoxControllerRightTriger.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(269, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "controllerLeftTriger";
            // 
            // textBoxControllerLeftTriger
            // 
            this.textBoxControllerLeftTriger.Location = new System.Drawing.Point(384, 9);
            this.textBoxControllerLeftTriger.Name = "textBoxControllerLeftTriger";
            this.textBoxControllerLeftTriger.Size = new System.Drawing.Size(100, 20);
            this.textBoxControllerLeftTriger.TabIndex = 0;
            // 
            // labelConnectionState
            // 
            this.labelConnectionState.AutoSize = true;
            this.labelConnectionState.Location = new System.Drawing.Point(39, 4);
            this.labelConnectionState.Name = "labelConnectionState";
            this.labelConnectionState.Size = new System.Drawing.Size(167, 13);
            this.labelConnectionState.TabIndex = 0;
            this.labelConnectionState.Text = "Connection State: Not Connected";
            // 
            // GCS_MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 544);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "GCS_MAIN";
            this.Text = "Group 4 - ZEPHYR - Ground Control Station";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCommands)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImagery)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewData;
        private System.Windows.Forms.DataGridView dataGridViewCommands;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBoxImagery;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox comboBoxServer;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Panel panel5;
        private Vlc.DotNet.Forms.VlcControl vlcControl;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxControllerLeftTriger;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxControllerAnalogRightX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxControllerAnalogLeftY;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxControllerRightTriger;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxLimitVertical;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxLimitHorizontal;
        private System.Windows.Forms.Label labelMotorVertical;
        private System.Windows.Forms.TextBox textBoxMotorVertical;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxMotorRight;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxMotorLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDN;
        private System.Windows.Forms.Button buttonDP;
        private System.Windows.Forms.Button buttonDG;
        private System.Windows.Forms.Button buttonDY;
        private System.Windows.Forms.Label labelConnectionState;
    }
}

